package com.paulganly.boxever.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.paulganly.boxever.Application;
import com.paulganly.boxever.model.FlightPath;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ShortestDurationServiceTest {
  
  @Autowired
  private ShortestDurationService shortestDurationService;
  
  @Test
  public void testGetShortestFlightDurationDubLhr() {
    shortestDurationService.calculateShortestPathsForSourceCity("DUB");
    List<FlightPath> shortestFlightDuration = shortestDurationService.getShortestFlightDuration("LHR");
    assertEquals(1, shortestFlightDuration.size());
    assertEquals(1, shortestFlightDuration.get(0).getDuration());
  }
  
  @Test
  public void testGetShortestFlightDurationDubSdy() {
    shortestDurationService.calculateShortestPathsForSourceCity("DUB");
    List<FlightPath> shortestFlightDuration = shortestDurationService.getShortestFlightDuration("SYD");
    assertEquals(3, shortestFlightDuration.size());
    Integer totalFlightDuration = shortestFlightDuration.stream().map(FlightPath::getDuration)
        .mapToInt(Integer::intValue).sum();
    assertEquals(21, totalFlightDuration);
  }

}
