package com.paulganly.boxever;

import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import com.paulganly.boxever.configuration.WorldMapConfiguration;
import com.paulganly.boxever.model.FlightPath;
import com.paulganly.boxever.services.ShortestDurationService;

@SpringBootApplication
@Import(WorldMapConfiguration.class)
public class Application implements CommandLineRunner {

  @Autowired
  public ShortestDurationService shortestDurationService;

  private static Logger logger = LoggerFactory.getLogger(Application.class);

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    if (validArgs(args)) {
      shortestDurationService.calculateShortestPathsForSourceCity(getSourceCityCode(args[0]));
      List<FlightPath> shortestFlightDuration =
          shortestDurationService.getShortestFlightDuration(getDestinationCityCode(args[0]));
      String response = buildResultsString(shortestFlightDuration);
      logger.info(response);
    } else {
      logger.error(
          "Please input a argument as departure city code and destination city code sepearted by a hyphen e.g. DUB-SDY.");
    }
  }

  private String buildResultsString(List<FlightPath> shortestFlightDuration) {
    String flightPathsString =
        shortestFlightDuration.stream().map(FlightPath::toString).collect(Collectors.joining("\n"));
    Integer totalFlightDuration = shortestFlightDuration.stream().map(FlightPath::getDuration)
        .mapToInt(Integer::intValue).sum();
    return new StringBuilder("\n").append(flightPathsString).append("\n").append("time: ")
        .append(totalFlightDuration).toString();
  }

  private String getDestinationCityCode(String string) {
    return string.split("-")[1];
  }

  private String getSourceCityCode(String arg) {
    return arg.split("-")[0];
  }

  private static boolean validArgs(String[] args) {
    if (args.length != 1 || !args[0].contains("-") || args[0].length() != 7) {
      return false;
    }
    return true;
  }

}
