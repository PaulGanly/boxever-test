package com.paulganly.boxever.model;

public class FlightPath {

  private final City source;
  private final City destination;
  private final int duration;

  public FlightPath(City source, City destination, int duration) {
    this.source = source;
    this.destination = destination;
    this.duration = duration;
  }

  public City getSource() {
    return source;
  }

  public City getDestination() {
    return destination;
  }

  public int getDuration() {
    return duration;
  }

  @Override
  public String toString() {
    return new StringBuilder(source.getCode()).append(" -- ").append(destination.getCode())
        .append(" (").append(duration).append(")").toString();
  }

}
