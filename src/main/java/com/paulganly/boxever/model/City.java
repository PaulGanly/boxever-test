package com.paulganly.boxever.model;

public class City {

  private String code;

  public City(String name) {
      this.code = name;
  }

  public String getCode() {
      return code;
  }

  public void setCode(String code) {
      this.code = code;
  }

}
