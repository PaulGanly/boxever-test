package com.paulganly.boxever.model;

import java.util.List;

public class WorldMap {

  private final List<City> cities;
  private final List<FlightPath> flightPaths;

  public WorldMap(List<City> cities, List<FlightPath> flightPaths) {
    this.cities = cities;
    this.flightPaths = flightPaths;
  }

  public List<City> getCities() {
    return cities;
  }

  public List<FlightPath> getFlightPaths() {
    return flightPaths;
  }

  public City findCityByCode(String departureCityCode) {
    return cities.stream().filter(city -> city.getCode().equals(departureCityCode)).findFirst()
        .get();
  }
}
