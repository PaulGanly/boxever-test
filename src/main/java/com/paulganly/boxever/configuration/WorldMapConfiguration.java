package com.paulganly.boxever.configuration;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import com.paulganly.boxever.model.City;
import com.paulganly.boxever.model.FlightPath;
import com.paulganly.boxever.model.WorldMap;

public class WorldMapConfiguration {

  @Bean
  public WorldMap worldMap() {

    City dub = new City("DUB");
    City cdg = new City("CDG");
    City ord = new City("ORD");
    City lhr = new City("LHR");
    City nyc = new City("NYC");
    City bos = new City("BOS");
    City bkk = new City("BKK");
    City lax = new City("LAX");
    City las = new City("LAS");
    City syd = new City("SYD");

    List<City> cities = new ArrayList<>();
    cities.add(dub);
    cities.add(cdg);
    cities.add(ord);
    cities.add(lhr);
    cities.add(nyc);
    cities.add(bos);
    cities.add(bkk);
    cities.add(lax);
    cities.add(las);
    cities.add(syd);

    List<FlightPath> flightPaths = new ArrayList<>();
    flightPaths.add(new FlightPath(dub, cdg, 2));
    flightPaths.add(new FlightPath(dub, lhr, 1));
    flightPaths.add(new FlightPath(cdg, bos, 6));
    flightPaths.add(new FlightPath(cdg, bkk, 9));
    flightPaths.add(new FlightPath(ord, las, 2));
    flightPaths.add(new FlightPath(lhr, nyc, 5));
    flightPaths.add(new FlightPath(nyc, las, 3));
    flightPaths.add(new FlightPath(bos, lax, 4));
    flightPaths.add(new FlightPath(lhr, bkk, 9));
    flightPaths.add(new FlightPath(bkk, syd, 11));
    flightPaths.add(new FlightPath(lax, las, 2));
    flightPaths.add(new FlightPath(dub, ord, 6));
    flightPaths.add(new FlightPath(lax, syd, 13));
    flightPaths.add(new FlightPath(las, syd, 14));

    return new WorldMap(cities, flightPaths);
  }
}
