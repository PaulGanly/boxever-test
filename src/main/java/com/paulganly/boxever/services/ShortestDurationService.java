package com.paulganly.boxever.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.paulganly.boxever.model.City;
import com.paulganly.boxever.model.FlightPath;
import com.paulganly.boxever.model.WorldMap;

@Service
public class ShortestDurationService {

  private final WorldMap worldMap;
  private Set<City> calculatedCities;
  private Set<City> uncalculatedCities;
  private Map<City, FlightPath> predecessors;
  private Map<City, Integer> duration;


  @Autowired
  public ShortestDurationService(WorldMap worldMap) {
    this.worldMap = worldMap;
  }

  public void calculateShortestPathsForSourceCity(String departureCityCode) {
    City departureCity = worldMap.findCityByCode(departureCityCode);
    initialiseValues(departureCity);
    while (!uncalculatedCities.isEmpty()) {
      City city = getClosestCity(uncalculatedCities);
      calculatedCities.add(city);
      uncalculatedCities.remove(city);
      findMinimalDurations(city);
    }
  }

  /**
   * Set the departure city (source) as the only initial uncalculated city. From here we will step
   * through each closest neighboring city and calculate shortest paths.
   * 
   * @param departureCity
   */
  private void initialiseValues(City departureCity) {
    calculatedCities = new HashSet<>();
    uncalculatedCities = new HashSet<>();
    duration = new HashMap<>();
    predecessors = new HashMap<>();
    duration.put(departureCity, 0);
    uncalculatedCities.add(departureCity);
  }

  private void findMinimalDurations(City city) {
    List<City> neighbouringCities = getNeighbours(city);
    for (City target : neighbouringCities) {
      if (knownShortestPathLongerThanNewPath(city, target)) {
        duration.put(target, getShortestDuration(city) + getDuration(city, target));
        predecessors.put(target, getMatchingFlightPath(city, target));
        uncalculatedCities.add(target);
      }
    }

  }

  private boolean knownShortestPathLongerThanNewPath(City city, City dest) {
    return getShortestDuration(dest) > (getShortestDuration(city) + getDuration(city, dest));
  }

  /**
   * Get the duration of the flight path between the 2 cities or Max Integer if none exists
   * 
   * @param city departure city
   * @param target
   * @return
   */
  private int getDuration(City city, City target) {
    FlightPath flightPath = getMatchingFlightPath(city, target);
    return flightPath != null ? flightPath.getDuration() : Integer.MAX_VALUE;
  }

  /**
   * Return a flight path matching the departure and arrival city or null if none exists
   * 
   * @param city departure city
   * @param destination destination city
   * @return a matching flight path
   */
  private FlightPath getMatchingFlightPath(City city, City destination) {
    for (FlightPath flightPath : worldMap.getFlightPaths()) {
      if (flightPath.getSource().equals(city) && flightPath.getDestination().equals(destination)) {
        return flightPath;
      }
    }
    return null;
  }

  /**
   * Get cities that there is a flight path to from the given city
   * 
   * @param city the city to find flight paths from
   * @return a list of neighbouring cities
   */
  private List<City> getNeighbours(City city) {
    List<City> neighbors = new ArrayList<>();
    for (FlightPath flightPath : worldMap.getFlightPaths()) {
      if (flightPath.getSource().equals(city) && !isSettled(flightPath.getDestination())) {
        neighbors.add(flightPath.getDestination());
      }
    }
    return neighbors;
  }

  /**
   * Step though the list of unsettled cities and choose the closest.
   * 
   * @param cities the unsettled cities
   * @return the closest (shortest duration path)
   */
  private City getClosestCity(Set<City> cities) {
    City closest = null;
    for (City city : cities) {
      if (closest == null) {
        closest = city;
      } else {
        if (getShortestDuration(city) < getShortestDuration(closest)) {
          closest = city;
        }
      }
    }
    return closest;
  }

  private boolean isSettled(City city) {
    return calculatedCities.contains(city);
  }

  private Integer getShortestDuration(City destination) {
    return (duration.get(destination) != null) ? duration.get(destination) : Integer.MAX_VALUE;
  }

  public List<FlightPath> getShortestFlightDuration(String destinationCityCode) {
    City destinationCity = worldMap.findCityByCode(destinationCityCode);
    LinkedList<FlightPath> path = new LinkedList<>();
    City step = destinationCity;
    // check if a path exists
    if (predecessors.get(step) == null) {
      return new ArrayList<>();
    }
    path.add(predecessors.get(step));
    while (predecessors.get(step) != null) {
      step = predecessors.get(step).getSource();
      if (predecessors.get(step) != null) {
        path.add(predecessors.get(step));
      }
    }
    // Put it into the correct order
    Collections.reverse(path);
    return path;
  }

}
