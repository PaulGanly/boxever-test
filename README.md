# Boxever, Shortest flight path test

Calculates the shortest path between two cities given the following available flights: | City Code |
| City Code |
| ---- |
 | DUB |
 | CDG |
 | ORD |
 | LHR |
 | NYC |
 | BOS |
 | BKK |
 | LAX |
 | LAS |
 | SYD |

## Features
Uses Dijkstras algorithm to calculate the shortest path between 2 cities. Details on Dijkstras algorithm here [Dijkstras](https://www.freecodecamp.org/news/dijkstras-shortest-path-algorithm-visual-introduction/)

## Installation

Requires [Maven](https://maven.apache.org/) to bulid.

Create the jar:

```sh
mvn clean package
```
Cd to the target folder then run the jar with the desired cities as an argument separated by a hyphen:

```sh
java -jar boxever-test-0.0.1-SNAPSHOT.jar DUB-SYD
```
